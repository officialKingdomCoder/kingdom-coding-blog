from rest_framework.test import APITestCase, APIRequestFactory

from api.views import PostViewSet, UserViewSet

from django.contrib.auth import get_user_model
from rest_framework.test import force_authenticate


class TestPostListView(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.view = PostViewSet.as_view({
            'get': 'list',
            'post': 'create'
        })
        self.uri = '/posts/'

        self.user = self.setup_user()

    @staticmethod
    def setup_user():
        User = get_user_model()
        return User.objects.create_user(
            'test',
            email='testuser@test.com',
            password='test'
        )


    def test_unauthenticated_list(self):
        request = self.factory.get(self.uri)
        response = self.view(request)
        self.assertEqual(response.status_code, 200,
                         'Expected Response Code 200, received {0} instead.'.format(response.status_code))

    def test_authenticated_list(self):
        request = self.factory.get(self.uri)
        force_authenticate(request, self.user)

        response = self.view(request)
        self.assertEqual(response.status_code, 200,
                         'Expected Response Code 200, received {0} instead.'.format(response.status_code))

    def test_unauthenticated_create(self):
        request = self.factory.post(self.uri, {
            'title': 'Test Title',
            'slug': 'test-slug',
            'content': 'Test Content',
            'author': self.user,

        })
        response = self.view(request)
        self.assertEqual(response.status_code, 403,
                         'Expected Response Code 403, received {0} instead.'.format(response.status_code))

    def test_authenticated_create(self):
        request = self.factory.post(self.uri, {
            'title': 'Test Title',
            'slug': 'test-slug',
            'content': 'Test Content',
            'author': self.user,

        })
        force_authenticate(request, self.user)
        response = self.view(request)
        self.assertEqual(response.status_code, 201,
                         'Expected Response Code 201, received {0} instead.'.format(response.status_code))


class TestPostDetailView(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.view = PostViewSet.as_view({
            'get': 'retrieve',
            'put': 'update',
            'patch': 'partial_update',
            'delete': 'destroy'
        })
        self.uri = '/posts/1'
        self.user = self.setup_user()

    @staticmethod
    def setup_user():
        User = get_user_model()
        return User.objects.create_user(
            'test',
            email='testuser@test.com',
            password='test'
        )
