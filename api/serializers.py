from rest_framework import serializers
from django.contrib.auth.models import User
from blogapp.models import Post


class UserSerializer(serializers.HyperlinkedModelSerializer):
    posts = serializers.HyperlinkedRelatedField(many=True, view_name='post-detail', read_only=True)

    class Meta:
        model = User
        fields = ['url', 'id', 'username', 'posts']


class PostSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    slug = serializers.ReadOnlyField()
    # slug = slugify(form.instance.title

    class Meta:
        model = Post
        fields = ['url', 'id', 'author',
                  'title', 'slug', 'content', 'status']
