from django.contrib.auth.models import User
from blogapp.models import Post

from api.serializers import UserSerializer, PostSerializer

from rest_framework import viewsets, permissions
from api.permissions import IsAuthorOrReadOnly

from django.template.defaultfilters import slugify


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsAuthorOrReadOnly]

    def perform_create(self, serializer):
        author = self.request.user
        title = self.request.data['title']
        
        slug = slugify("{title} by {author}".format(title=title, author=author.username))

        serializer.save(author=author, slug=slug)
