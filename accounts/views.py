# from django.shortcuts import render

# Create your views here.
from django.contrib.auth import authenticate, login
# from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group
from django.urls import reverse_lazy
from django.views import generic
from django.http import HttpResponseRedirect

from .forms import CustomSignUpForm


class SignUp(generic.CreateView):
    form_class = CustomSignUpForm
    success_url = reverse_lazy('home')
    template_name = 'registration/signup.html'

    def form_valid(self, form):
        # save new user
        form.save()

        # get username and password
        username = self.request.POST['username']
        password = self.request.POST['password1']

        # authenticate user
        user = authenticate(username=username, password=password)
        user.save()

        # assign privileges
        group = Group.objects.get(name='Author')
        user.groups.add(group)

        # log user in
        login(self.request, user)
        # this is the correct version but it is not working
        # return HttpResponseRedirect(self.get_success_url)
        return HttpResponseRedirect(reverse_lazy('home'))
