from django.urls import path

from django.contrib.auth import views as auth_views
from . import views
from .forms import CustomLoginForm


urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('login/', auth_views.LoginView.as_view(authentication_form=CustomLoginForm),  name='login'),
]
