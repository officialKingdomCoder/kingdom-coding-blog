import django_tables2 as tables
from django_tables2.utils import A
from blogapp.models import Post


class PostTable(tables.Table):
    title = tables.LinkColumn('edit_post', kwargs={'slug': A('slug')})
    
    class Meta:
        model = Post
        fields = ('title', 'created_on', 'updated_on', 'status')
        empty_text = "You have no posts yet!"
