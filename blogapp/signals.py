from django.db.models.signals import post_save
from django.dispatch import receiver
from guardian.shortcuts import assign_perm
from blogapp.models import Post


@receiver(post_save, sender=Post)
def set_permission(sender, instance, **kwargs):
    """Add object specific permission to the author"""
    assign_perm(
        "change_post",  # The permission we want to assign.
        instance.author,  # The user object.
        instance  # The object we want to assign the permission to.
    )
