from django.test import SimpleTestCase
from django.urls import reverse, resolve


# Create your tests here.
class TestUrls(SimpleTestCase):

    def test_posts_url_resolves(self):
        url = reverse('posts')
        self.assertEquals(resolve(url).view_name, 'posts')

    def test_new_post_url_resolves(self):
        url = reverse('new_post')
        self.assertEquals(resolve(url).view_name, 'new_post')

    def test_edit_post_url_resolves(self):
        url = reverse('edit_post', kwargs={'slug': 'test-slug'})
        self.assertEquals(resolve(url).view_name, 'edit_post')

    def test_home_url_resolves(self):
        url = reverse('home')
        self.assertEquals(resolve(url).view_name, 'home')

    def test_post_detail_url_resolves(self):
        url = reverse('post_detail', kwargs={'slug': 'test-slug'})
        self.assertEquals(resolve(url).view_name, 'post_detail')
