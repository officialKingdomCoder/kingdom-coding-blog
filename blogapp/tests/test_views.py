from django.test import TestCase, Client
from django.urls import reverse
from blogapp.models import Post
import json

from django.contrib.auth import get_user_model


# Create your tests here.
class TestBlogViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.post_list_url = reverse('home')
        self.post_detail_url = reverse('post_detail', kwargs={'slug': 'test-slug'})
        Post.objects.create(
            title='Test Title',
            slug='test-slug',
            content='Test Content',
            status=1,
            author=self.setup_user()
        )

    @staticmethod
    def setup_user():
        User = get_user_model()
        return User.objects.create_user(
            'test',
            email='testuser@test.com',
            password='test'
        )

    def test_post_list(self):
        response = self.client.get(self.post_list_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_post_detail(self):
        response = self.client.get(self.post_detail_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'post_detail.html')


# Create your tests here.
class TestDashboardViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.author = self.setup_user(username='author', email='author@example.com', password='author')
        self.another_user = self.setup_user(username='another_user', email='another_user@example.com', password='another_user')
        self.post_list_url = reverse('posts')
        self.post_create_url = reverse('new_post')
        self.post_update_url = reverse('edit_post', kwargs={'slug': 'test-slug'})

        self.new_post = {
            'title': 'New Post Title',
            'content': 'New Post Content',
            'status': 1
        }

        Post.objects.create(
            title='Test Title',
            slug='test-slug',
            content='Test Content',
            status=1,
            author=self.author
        )

        self.update_post_content_author = {
            'title': 'Updated Post Title',
            'slug': 'updated-post-slug',
            'content': 'Updated Post Content',
            'status': 1
        }

        self.update_post_content_another_user = {
            'title': 'Updated Post Title',
            'slug': 'updated-post-slug',
            'content': 'Updated Post Content',
            'status': 1
        }

    @staticmethod
    def setup_user(username, email, password):
        User = get_user_model()
        return User.objects.create_user(
            username=username,
            email=email,
            password=password
        )

    def test_unauthenticated_post_list(self):
        response = self.client.get(self.post_list_url)

        self.assertEquals(response.status_code, 302)

    def test_unauthenticated_post_create(self):
        response = self.client.post(self.post_create_url, self.new_post)

        self.assertEquals(response.status_code, 302)

    def test_unauthenticated_post_update(self):
        response = self.client.put(self.post_update_url, self.update_post_content_author)

        self.assertEquals(response.status_code, 302)

    # authenticated requests
    def test_authenticated_post_list(self):
        self.client.login(username='author', password='author')
        response = self.client.get(self.post_list_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'dashboard/posts.html')

    def test_authenticated_post_create(self):
        self.client.login(username='author', password='author')
        response = self.client.post(self.post_create_url, self.new_post)

        self.assertEquals(response.status_code, 302)

    def test_author_post_update(self):
        self.client.login(username='author', password='author')
        response = self.client.put(self.post_update_url, self.update_post_content_author)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'dashboard/edit_post.html')

    def test_another_user_post_update(self):
        self.client.login(username='another_user', password='another_user')
        response = self.client.put(self.post_update_url, self.update_post_content_another_user)

        self.assertEquals(response.status_code, 302)
