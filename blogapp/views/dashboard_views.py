from django.views import generic
from django.views.generic.edit import CreateView, UpdateView
from django_tables2 import RequestConfig
from blogapp.tables import PostTable
from blogapp.models import Post
from blogapp.forms import PostForm

from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from guardian.mixins import PermissionRequiredMixin

from django.shortcuts import get_object_or_404


# Create your views here.
class PostList(LoginRequiredMixin, generic.ListView):
    template_name = 'dashboard/posts.html'
    paginate_by = 20
    ordering = ['-created_on']

    def get_queryset(self):
        return Post.objects.filter(author=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(PostList, self).get_context_data(**kwargs)
        table = PostTable(self.get_queryset())
        RequestConfig(self.request).configure(table)
        context['table'] = table
        return context


class PostUpdate(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    form_class = PostForm
    template_name = 'dashboard/edit_post.html'
    success_url = reverse_lazy('posts')
    permission_required = 'change_post'

    def get_object(self):
        """Returns the Post instance that the view displays"""
        return get_object_or_404(Post, slug=self.kwargs.get("slug"))


class PostCreate(LoginRequiredMixin, CreateView):
    model = Post
    form_class = PostForm
    # fields = ['title', 'slug', 'author', 'content', 'status']
    template_name = 'dashboard/new_post.html'
    success_url = reverse_lazy('posts')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(PostCreate, self).form_valid(form)
