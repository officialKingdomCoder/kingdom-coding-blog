from .views import blog_views, dashboard_views
from django.urls import path

urlpatterns = [
    path('posts/', dashboard_views.PostList.as_view(), name='posts'),
    path('add/', dashboard_views.PostCreate.as_view(), name='new_post'),
    path('edit/<slug:slug>/', dashboard_views.PostUpdate.as_view(), name='edit_post'),


    path('', blog_views.PostList.as_view(), name='home'),
    path('<slug:slug>/', blog_views.PostDetail.as_view(), name='post_detail'),
]
